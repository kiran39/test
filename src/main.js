import Vue from 'vue' //Import Vue
import App from './App.vue' //Import Main App file

Vue.config.productionTip = false

new Vue({
  el: '#app',
  render: h => h(App)
});
